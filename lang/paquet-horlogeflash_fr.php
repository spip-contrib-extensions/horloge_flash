<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-horlogeflash
// Langue: fr
// Date: 24-06-2020 11:18:52
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// H
	'horlogeflash_description' => 'Modèle d\'horloge recevant la TimeZone en paramètre',
	'horlogeflash_slogan' => 'Modèle d\'horloge recevant la TimeZone en paramètre',
);
?>